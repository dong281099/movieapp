//
//  HomeModel.swift
//  movieApp
//
//  Created by Nguyen Huy Dong on 7/12/21.
//

import Foundation
import RealmSwift

typealias Home = HomeModel
typealias fav = FavouritesViewController

struct HomeModel: Decodable {
    let adult: Bool?
    var backdropPath: String?
    let id: Int?
    let mediaType: String?
    let originalLanguage: String?
    let originalTitle: String?
    let overview: String?
    let popularity: Float?
    let posterPath: String?
    let releaseDate: String?
    let title: String?
    let video: Bool?
    let voteAverage: Double?
    let voteCount: Int?

    private enum CodingKeys: String, CodingKey {
        case adult
        case backdropPath = "backdrop_path"
        case id
        case mediaType = "media_type"
        case originalLanguage = "original_language"
        case originalTitle = "original_title"
        case overview
        case popularity
        case posterPath = "poster_path"
        case releaseDate = "release_date"
        case title
        case video
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
    }
}

class Favourite: Object{
    @objc dynamic var id: Int = 0
    @objc dynamic var voteCount: Double = 0
    @objc dynamic var favourite: Bool = false
    override static func primaryKey() -> String? {
        "id"
    }
    
    func initLoad(_ json:  [String: Any]) -> Favourite{
        if let temp = json["id"] as? Int { id = temp }
        if let temp = json["voteCount"] as? Double { voteCount = temp }
        if let temp = json["favourite"] as? Bool { favourite = temp }
        
        return self
    }
    
}




