//
//  User.swift
//  movieApp
//
//  Created by Nguyen Huy Dong on 8/18/21.
//

import Foundation
import RealmSwift

class User: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var firstName: String = ""
    @objc dynamic var lastName: String = ""
    @objc dynamic var email: String = ""
    @objc dynamic var password: String = ""
    @objc dynamic var isLogin: Bool = false
    
    override static func primaryKey() -> String? {
        "email"
    }
    
    func initLoad(_ json:  [String: Any]) -> User{
        if let temp = json["id"] as? Int { id = temp }
        if let temp = json["firstName"] as? String { firstName = temp }
        if let temp = json["lastName"] as? String { lastName = temp }
        if let temp = json["email"] as? String { email = temp }
        if let temp = json["password"] as? String { password = temp }
        if let temp = json["isLogin"] as? Bool { isLogin = temp }
        
        return self
    }
}
