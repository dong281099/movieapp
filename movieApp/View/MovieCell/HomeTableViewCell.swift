//
//  HomeTableViewCell.swift
//  movieApp
//
//  Created by Nguyen Huy Dong on 7/12/21.
//

import UIKit
import RealmSwift

typealias SeeAllClosure = ((_ index: Int?) -> Void)

protocol HomeTableViewCellDelegate: class {
    func didSelectMovie(movie: HomeModel)
}

class HomeTableViewCell: UITableViewCell, UISearchBarDelegate {
    
    //IBOutlet
    @IBOutlet weak var viewSeeAll: UIView!
    @IBOutlet public weak var collectionView: UICollectionView!
    @IBOutlet public weak var headerLabel: UILabel!
    
    //variables
    weak var delegate: HomeTableViewCellDelegate?
    var onClickSeeAllClosure: SeeAllClosure?
    var index : Int?
    
    var movie: HomeModel?
    var homeModel : [HomeModel] = []{
        didSet{
            collectionView.delegate = self
            collectionView.dataSource = self
            collectionView.register(UINib(nibName: Constants.homeCollectionViewCell, bundle: Bundle.main), forCellWithReuseIdentifier: Constants.homeCollectionViewCell)
            collectionView.backgroundColor = .black
            collectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func seeAll(_ sender: Any) {
        onClickSeeAllClosure?(index)
    }
}

extension HomeTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return homeModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.homeCollectionViewCell, for: indexPath) as? HomeCollectionViewCell
        else {
            return UICollectionViewCell()
        }
        cell.movieName.text = homeModel[indexPath.item].originalTitle
        let defaultUrl = Constants.IMAGES_BASE_URL
        let url = defaultUrl + (homeModel[indexPath.item].posterPath)!
        cell.img.downloaded(from: url)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3, height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didSelectMovie(movie: homeModel[indexPath.row])
    }
}

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: ContentMode = .scaleAspectFill) {
            contentMode = mode
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard
                    let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                    let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                    let data = data, error == nil,
                    let image = UIImage(data: data)
                else { return }
                DispatchQueue.main.async() { [weak self] in
                    self?.image = image
                }
            }.resume()
        }

    func downloaded(from link: String, contentMode mode: ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
    
    func imageFromServerURL(_ URLString: String, placeHolder: UIImage?) {
        
        self.kf.setImage(with: URL(string: URLString))
    }
}

extension HomeTableViewCell {
    func nowPlaying(movies: [HomeModel]) {
        headerLabel.text = "Now Playing"
        self.homeModel = movies
    }
    func highestRated(movies: [HomeModel]){
        headerLabel.text = "Highest Rated"
        self.homeModel = movies.sorted{
            $0.voteAverage! < $1.voteAverage! }
    }
    func upComing(movies: [HomeModel]){
        headerLabel.text = "Up Coming"
        self.homeModel = movies.sorted{
            $0.id! < $1.id! }
    }
    func popular(movies: [HomeModel]){
        headerLabel.text = "Popular"
        self.homeModel = movies.sorted{
            $0.popularity! < $1.popularity! }
    }
}
