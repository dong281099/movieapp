//
//  SeeAllTableViewCell.swift
//  movieApp
//
//  Created by Nguyen Huy Dong on 8/17/21.
//

import UIKit

class SeeAllTableViewCell: UITableViewCell {

    @IBOutlet weak var imgSeeAll: UIImageView!
    @IBOutlet weak var nameSeeAll: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
