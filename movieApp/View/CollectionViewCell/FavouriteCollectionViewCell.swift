//
//  FavouriteCollectionViewCell.swift
//  movieApp
//
//  Created by Nguyen Huy Dong on 8/14/21.
//

import UIKit

class FavouriteCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgMovie: UIImageView!
    @IBOutlet weak var nameMovie: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
