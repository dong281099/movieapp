//
//  SignUpViewController.swift
//  movieApp
//
//  Created by Nguyen Huy Dong on 8/19/21.
//

import UIKit
import RealmSwift

class SignUpViewController: UIViewController {
    
    let user1 = User()
    
    @IBOutlet weak var firstNameSignUp: UITextField!
    @IBOutlet weak var lastNameSignUp: UITextField!
    @IBOutlet weak var emailSignUp: UITextField!
    @IBOutlet weak var passwordSignUp: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        navigationController?.navigationBar.isHidden = false
        setUpElements()
    }
    
    func setUpElements() {
        
        // Hide the error label
        errorLabel.alpha = 0
        
        // Style the elements
        Utilities.styleTextField(firstNameSignUp)
        Utilities.styleTextField(lastNameSignUp)
        Utilities.styleTextField(emailSignUp)
        Utilities.styleTextField(passwordSignUp)
        Utilities.styleFilledButton(signUpButton)
    }
    
    func validateFields() -> String? {
        if firstNameSignUp.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            lastNameSignUp.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            emailSignUp.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            passwordSignUp.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return "Please fill in all fields."
        }
        
        let cleanedPassword = passwordSignUp.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if Utilities.isPasswordValid(cleanedPassword) == false {
            return "Please make sure your password is at least 8 characters, contains a special character and a number."
        }
        return nil
    }
    
    @IBAction func signUpTapped(_ sender: UIButton) {
        let error = validateFields()
        if error != nil {
            showError(error!)
        } else {
            // add data of account to Realm database
            add()
        }
    }
    
    func add() {
        user1.id = incrementID()
        user1.firstName = firstNameSignUp.text!
        user1.lastName = lastNameSignUp.text!
        user1.email = emailSignUp.text!
        user1.password = passwordSignUp.text!
        
        let realm = try! Realm()
        try! realm.write {
            realm.add(user1)
        }
        guard let loginVC = storyboard?.instantiateViewController(identifier: Constants.loginViewController) as? LoginViewController else {return}
        navigationController?.pushViewController(loginVC, animated: true)
    }
    
    func incrementID() -> Int {
        let realm = try! Realm()
        return (realm.objects(User.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    func showError(_ message: String) {
        errorLabel.text = message
        errorLabel.alpha = 1
    }
}
