//
//  LoginViewController.swift
//  movieApp
//
//  Created by Nguyen Huy Dong on 8/19/21.
//

import UIKit
import RealmSwift

class LoginViewController: UIViewController {
    
    var user1 = User()

    @IBOutlet weak var emailLogin: UITextField!
    @IBOutlet weak var passwordLogin: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    @IBAction func register(_ sender: Any) {
        guard let registerVC = storyboard?.instantiateViewController(identifier: Constants.signUpViewController) as? SignUpViewController else {return}
         navigationController?.pushViewController(registerVC, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        navigationController?.navigationBar.isHidden = false
        setUpElements()
    }
    
    func setUpElements() {
        
        // Hide the error label
        errorLabel.alpha = 0
        
        // Style the elements
        Utilities.styleTextField(emailLogin)
        Utilities.styleTextField(passwordLogin)
        Utilities.styleFilledButton(loginButton)
    }

    @IBAction func loginTapped(_ sender: UIButton) {
        let realm = try! Realm()
        let userData = realm.objects(User.self)
        
        let email = emailLogin.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = passwordLogin.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        for item in userData {
            if email == item.email && password == item.password {
                try! realm.write {
                    item.isLogin = true
                }
                guard let loginVC = storyboard?.instantiateViewController(identifier: Constants.homeTabBar) as? homeTabBarController else {return}
                 navigationController?.pushViewController(loginVC, animated: true)
            }
        }
    }
}
