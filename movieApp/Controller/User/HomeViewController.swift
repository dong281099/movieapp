//
//  HomeViewController.swift
//  movieApp
//
//  Created by Nguyen Huy Dong on 8/19/21.
//

import UIKit
import AVFoundation 
import AVKit

class HomeViewController: UIViewController {

    var videoPlayer:AVPlayer?
    var videoPlayerLayer:AVPlayerLayer?
    private var playerLayer = AVPlayerLayer()
    private var playerLooper: AVPlayerLooper?
    
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpElements()
        navigationController?.navigationBar.isHidden = true
        loginButton.addTarget(self, action: #selector(transitionToLogin), for: .touchUpInside)
        signUpButton.addTarget(self, action: #selector(transitionToSignUp), for: .touchUpInside)
    }
    
   @objc func transitionToLogin() {
        let loginVC = storyboard?.instantiateViewController(identifier: Constants.loginViewController) as? LoginViewController
        navigationController?.pushViewController(loginVC!, animated: true)
    }
    
    @objc func transitionToSignUp() {
         let signUpVC = storyboard?.instantiateViewController(identifier: Constants.signUpViewController) as? SignUpViewController
         navigationController?.pushViewController(signUpVC!, animated: true)
     }
    override func viewWillAppear(_ animated: Bool) {
        setUpVideo()
    }

    func setUpElements() {
        Utilities.styleFilledButton(signUpButton)
        Utilities.styleHollowButton(loginButton)
    }
    
    func setUpVideo() {
        
        // Get the path to the resource in the bundle
        let bundlePath = Bundle.main.path(forResource: "beach", ofType: "mp4")

        guard bundlePath != nil else {
            return
        }

        // Create a URL from it
        let url = URL(fileURLWithPath: bundlePath!)

        // Create the video player item
        let item = AVPlayerItem(url: url)

        // Create the player
//        let player = AVQueuePlayer(playerItem: item)
        videoPlayer = AVPlayer(playerItem: item)
        playerLayer.player = videoPlayer
        playerLayer.videoGravity = .resizeAspectFill
        
        // Create the layer
        videoPlayerLayer = AVPlayerLayer(player: videoPlayer!)
        
        //Loop
//        playerLooper = AVPlayerLooper(player: player, templateItem: item)

        // Adjust the size and frame
        videoPlayerLayer?.frame = CGRect(x: -self.view.frame.size.width*1, y: 0, width: self.view.frame.size.width*3, height: self.view.frame.size.height)
        
        view.layer.insertSublayer(videoPlayerLayer!, at: 0)

        // Add it to the view and play it
        videoPlayer?.playImmediately(atRate: 0.3)
    }
}
