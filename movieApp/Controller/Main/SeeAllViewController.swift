//
//  SeeAllViewController.swift
//  movieApp
//
//  Created by Nguyen Huy Dong on 8/17/21.
//

import UIKit
import Kingfisher
import RealmSwift

class SeeAllViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(UINib(nibName: Constants.seeAllTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.seeAllTableViewCell)
            tableView.register(UINib(nibName: Constants.searchTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.searchTableViewCell)
            tableView.delegate = self
            tableView.dataSource = self
            tableView.backgroundColor = Color.UIColorFromRGB(rgbValue: 0x444444)
            tableView.tableHeaderView?.backgroundColor = .black
            tableView.reloadData()
        }
    }
    
    var homeModel : [HomeModel] = [HomeModel]()
    var seeAllData : HomeModel!
    var filteredTableData: [HomeModel] = [HomeModel]()
    var searchActive = false
    let cellSpacingHeight: CGFloat = 5.0
    var searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSearch()
    }
}

extension SeeAllViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchActive {
            return filteredTableData.count
        }
        else {
            return homeModel.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if searchActive {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.searchTableViewCell) as? SearchTableViewCell
            else{
                return UITableViewCell()
            }
            let url = URL(string: Constants.IMAGES_BASE_URL + (filteredTableData[indexPath.row].posterPath ?? ""))
            cell.imgMovie.kf.setImage(with: url)
            cell.backgroundColor = Color.UIColorFromRGB(rgbValue: 0x333333)
            cell.movieName.text = filteredTableData[indexPath.row].title
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.seeAllTableViewCell) as? SeeAllTableViewCell
            else{
                return UITableViewCell()
            }
            let url = URL(string: Constants.IMAGES_BASE_URL + (homeModel[indexPath.row].posterPath ?? ""))
            cell.imgSeeAll.kf.setImage(with: url)
            cell.nameSeeAll.text = homeModel[indexPath.row].title
            cell.backgroundColor = Color.UIColorFromRGB(rgbValue: 0x003333)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return cellSpacingHeight
        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searchActive {
                guard let detailVC = storyboard?.instantiateViewController(identifier: Constants.detailViewController) as? DetailViewController else {return}
                detailVC.homeModel = filteredTableData[indexPath.row]
                navigationController?.pushViewController(detailVC, animated: true)
            
        } else {
            guard let detailVC = storyboard?.instantiateViewController(identifier: Constants.detailViewController) as? DetailViewController else {return}
            detailVC.homeModel = homeModel[indexPath.row]
            navigationController?.pushViewController(detailVC, animated: true)
        }
    }
    
}

extension SeeAllViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchString = searchController.searchBar.text!
        if !searchString.isEmpty {
            searchActive = true
            filteredTableData.removeAll()
            for item in homeModel {
                if item.title?.uppercased().contains(searchString.uppercased()) == true{
                    filteredTableData.append(item)
                }
            }
        }
        else {
            searchActive = false
            filteredTableData.removeAll()
            filteredTableData = homeModel
        }
        tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        filteredTableData.removeAll()
        tableView.reloadData()
    }
}

extension SeeAllViewController {
    func configureSearch() {
        self.searchController = ({
            let searchController = UISearchController(searchResultsController: nil)
            searchController.searchResultsUpdater = self
            searchController.obscuresBackgroundDuringPresentation = false
            searchController.searchBar.placeholder = "Type name of movie"
            searchController.searchBar.sizeToFit()
            searchController.searchBar.searchTextField.textColor = Color.UIColorFromRGB(rgbValue: 0xFFFFFF)
            searchController.searchBar.searchTextField.backgroundColor = Color.UIColorFromRGB(rgbValue: 0x666666)
            searchController.searchBar.barTintColor = Color.UIColorFromRGB(rgbValue: 0x444444)
            tableView.tableHeaderView = searchController.searchBar
            definesPresentationContext = true
            navigationItem.hidesSearchBarWhenScrolling = false
            return searchController
        })()
    }
}

extension SeeAllViewController {
    func nowPlaying(movies: [HomeModel]) {
        self.homeModel = movies
    }
    func highestRated(movies: [HomeModel]){
        self.homeModel = movies.sorted{
            $0.voteAverage! < $1.voteAverage! }
    }
    func upComing(movies: [HomeModel]){
        self.homeModel = movies.sorted{
            $0.id! < $1.id! }
    }
    func popular(movies: [HomeModel]){
        self.homeModel = movies.sorted{
            $0.popularity! < $1.popularity! }
    }
}
