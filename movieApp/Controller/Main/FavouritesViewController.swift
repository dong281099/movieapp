//
//  FavouritesViewController.swift
//  movieApp
//
//  Created by Nguyen Huy Dong on 8/13/21.
//

import UIKit
import RealmSwift

class FavouritesViewController: UIViewController {
    
    //IBOutlet
    @IBOutlet weak var collectionView: UICollectionView!
    
    //Variables
    var homeModel: [HomeModel] =  [HomeModel]()
    var filteredFavouriteData: [HomeModel] = [HomeModel]()
    var favourite = [Favourite]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        title = "FAVOURITES"
        loadJson()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: Constants.favouriteCollectionViewCell, bundle: Bundle.main), forCellWithReuseIdentifier: Constants.favouriteCollectionViewCell)
    }
    
    func getFavourite() -> [Favourite] {
        let realm = try! Realm()
        let film = realm.objects(Favourite.self)
        var list:[Favourite] = [Favourite]()
        for item in film {
            if item.favourite {
                list.append(item)
            }
        }
        return list
    }
    
    func setupFavourite() -> [HomeModel]{
        for item in homeModel {
            for item2 in favourite {
                if item.id == item2.id {
                    filteredFavouriteData.append(item)
                }
            }
        }
        return filteredFavouriteData
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        filteredFavouriteData.removeAll()
        self.favourite = self.getFavourite()
        self.filteredFavouriteData = self.setupFavourite()
        collectionView.reloadData()
    }
}

extension FavouritesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filteredFavouriteData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.favouriteCollectionViewCell, for: indexPath) as? FavouriteCollectionViewCell
       else {
           return UICollectionViewCell()
       }
        cell.contentView.layer.cornerRadius = 2.0
        cell.layer.shadowOffset = CGSize(width:0,height: 2.0)
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false;
        cell.nameMovie.text = filteredFavouriteData[indexPath.item].title
        let defaultUrl = Constants.IMAGES_BASE_URL
        let url = defaultUrl + (filteredFavouriteData[indexPath.item].posterPath ?? "")
        cell.imgMovie.downloaded(from: url)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 2 - 40, height: collectionView.frame.height / 3)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let detailVC = storyboard?.instantiateViewController(identifier: Constants.detailViewController) as? DetailViewController else {return}
        detailVC.homeModel = filteredFavouriteData[indexPath.item]
        navigationController?.pushViewController(detailVC, animated: true)
    }
}

extension FavouritesViewController {
    func loadJson() {
        if let path = Bundle.main.path(forResource: "Movie", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                let jsonData = try JSONSerialization.data(withJSONObject: jsonResult, options: .prettyPrinted)
                let jsonDecoder = JSONDecoder()
                jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
                homeModel = try JSONDecoder().decode([HomeModel].self, from: jsonData)
            } catch {
                
            }
        }
    }
}
