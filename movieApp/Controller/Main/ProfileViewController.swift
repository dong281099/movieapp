//
//  ProfileViewController.swift
//  movieApp
//
//  Created by Nguyen Huy Dong on 8/24/21.
//

import UIKit
import RealmSwift

class ProfileViewController: UIViewController {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var imgProfile: UIImageView! {
        didSet {
            imgProfile.layer.cornerRadius = imgProfile.frame.size.height / 2
            imgProfile.clipsToBounds = true
        }
    }
    
    
    @IBAction func logoutButton(_ sender: Any) {
        let realm = try! Realm()
        let userData = realm.objects(User.self)
        for item in userData {
            if item.isLogin {
                try! realm.write {
                    item.isLogin = false
                }
            }
        }
        guard let loginVC = storyboard?.instantiateViewController(identifier: Constants.loginViewController) as? LoginViewController else {return}
        navigationController?.pushViewController(loginVC, animated: true)
    }
    
    var profile1 : User!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let realm = try! Realm()
        let userData = realm.objects(User.self)
        for item in userData {
            if item.isLogin {
                name.text = item.email
            }
        }
    }

}
