//
//  DetailViewController.swift
//  movieApp
//
//  Created by Nguyen Huy Dong on 7/19/21.
//
import Foundation
import UIKit
import Kingfisher
import HCSStarRatingView
import RealmSwift

class DetailViewController: UIViewController {
    
    //MARK: IBOutlet
    @IBOutlet weak var viewMovie: UIView!
    @IBOutlet private weak var imgDetail: UIImageView!
    @IBOutlet weak var movieName: UILabel!
    @IBOutlet weak var rateBar: HCSStarRatingView!
    @IBOutlet weak var overview: UILabel!
    @IBOutlet weak var buttonFav: UIButton!
    
    // MARK: Varibales
    
    var homeModel : HomeModel!
    var filtered: [HomeModel] = [HomeModel]()
    var favourite1 = [Favourite]()
    var favourite = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        add()
        setupNavigationBar()
        setupViews(movie: homeModel)
        self.imgDetail = imgDetail.roundImage
        setupFavourite()
        rateBar?.addTarget(self, action: #selector(setupRateBar), for: .touchUpInside)
        showRateBar()
    }
    
    //IBAction
    // set favourite status
    @IBAction func isFavourite(_ sender: UIButton) {
        let realm = try! Realm()
        let fav = realm.objects(Favourite.self)
        for item in fav {
            if item.id == homeModel.id {
                if  !item.favourite{
                    buttonFav.setImage(UIImage(systemName: "heart.fill"), for: .normal)
                    buttonFav.tintColor = .red
                    favourite = true
                    try! realm.write {
                        item.favourite = favourite
                    }
                    self.showToast(message: "You like this movie")
                    
                } else {
                    buttonFav.setImage(UIImage(systemName: "heart"), for: .normal)
                    favourite = false
                    try! realm.write {
                        item.favourite = favourite
                    }
                }
            }
        }
    }
    
    func setupFavourite() {
        let realm = try! Realm()
        let fav = realm.objects(Favourite.self)
        for item in fav {
            if item.id == homeModel.id{
                if item.favourite {
                    buttonFav.setImage(UIImage(systemName: "heart.fill"), for: .normal)
                    buttonFav.tintColor = .red
                }
            }
        }
    }
    
    func showRateBar() {
        let realm = try! Realm()
        let rateBar1 = realm.objects(Favourite.self)
        for item in rateBar1 {
            if item.id == homeModel.id {
                if item.voteCount != 0 {
                    rateBar.value = CGFloat(item.voteCount)
                    rateBar.tintColor = Color.UIColorFromRGB(rgbValue: 0x99FF33)
                }
            }
        }
    }
    
    @objc func setupRateBar() {
        let realm = try! Realm()
        let voteCount = realm.objects(Favourite.self)
        for item in voteCount {
            if item.id == homeModel.id {
                try! realm.write {
                    item.voteCount = Double(rateBar.value)
                }
            }
        }
    }
    
    func setupViews(movie:Home){
        let defaultUrl = Constants.IMAGES_BASE_URL
        let url = URL(string: defaultUrl + (movie.posterPath ?? ""))
        imgDetail.kf.setImage(with: url)
        movieName.text = movie.title?.uppercased()
        overview.text = movie.overview
    }
    
    
    func setupNavigationBar(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationItem.backBarButtonItem?.title = ""
        self.navigationController?.navigationBar.tintColor = .white
    }
    
    func add() {
        let realm = try! Realm()
        let data = Favourite()
        let favData = realm.objects(Favourite.self)
        for item in favData {
            if ( item.id == homeModel.id) {
                data.favourite = item.favourite
                data.voteCount = item.voteCount
            } else {
                data.id = homeModel.id!
            }
        }
        try! realm.write {
            realm.add(data, update: .all)
        }
    }
    
    func showToast(message : String) {

        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75 , y: self.view.frame.size.height/2 - 20, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.5, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}
