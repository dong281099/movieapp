//
//  ViewController.swift
//  movieApp
//
//  Created by Nguyen Huy Dong on 7/11/21.
//

import UIKit
import FSPagerView
import Kingfisher
import RealmSwift

class ViewController: UIViewController {
    //MARK: IBOutlet
    @IBOutlet weak var tableView: UITableView!{
        didSet {
            tableView.register(UINib(nibName: Constants.homeTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.homeTableViewCell)
            tableView.register(UINib(nibName: Constants.searchTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.searchTableViewCell)
            tableView.dataSource = self
            tableView.delegate = self
            tableView.backgroundColor = .black
            tableView.tableHeaderView?.backgroundColor = .black
            tableView.reloadData()
        }
    }
    
    //MARK: Variables
    var homeModel: [HomeModel] = [HomeModel]()
    var filteredTableData: [HomeModel] = [HomeModel]()
    var selectedMovie : HomeModel?
    var searchActive = false
    var pagerView = FSPagerView()
    var searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadJson()
        headerTable()
        configureSearch()
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchActive {
            return filteredTableData.count
        }
        else {
            return 1
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 180.0 : 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        // Swift 4.2 onwards
        if searchActive {
            return 150
        }
        return 235
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if searchActive {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.searchTableViewCell) as? SearchTableViewCell
            else{
                return UITableViewCell()
            }
            let url = URL(string: Constants.IMAGES_BASE_URL + (filteredTableData[indexPath.row].posterPath ?? ""))
            cell.imgMovie.kf.setImage(with: url)
            cell.backgroundColor = Color.UIColorFromRGB(rgbValue: 0x333333)
            cell.movieName.text = filteredTableData[indexPath.row].title
            return cell
        }else{
            switch indexPath.section {
            case 0:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.homeTableViewCell) as? HomeTableViewCell else{ return UITableViewCell() }
                cell.nowPlaying(movies: homeModel)
                cell.backgroundColor = .black
                cell.index = indexPath.section
                cell.onClickSeeAllClosure = {index in
                    if let indexSA = index {
                        self.moveSeeAll(index: indexSA)
                    }
                }
                cell.delegate = self
                return cell
            case 1:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.homeTableViewCell) as? HomeTableViewCell else{ return UITableViewCell() }
                cell.highestRated(movies: homeModel)
                cell.backgroundColor = .black
                cell.index = indexPath.section
                cell.onClickSeeAllClosure = {index in
                    if let indexSA = index {
                        self.moveSeeAll(index: indexSA)
                    }
                }
                cell.delegate = self
                return cell
            case 2:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.homeTableViewCell) as? HomeTableViewCell else{ return UITableViewCell() }
                cell.upComing(movies: homeModel)
                cell.backgroundColor = .black
                cell.index = indexPath.section
                cell.onClickSeeAllClosure = {index in
                    if let indexSA = index {
                        self.moveSeeAll(index: indexSA)
                    }
                }
                cell.delegate = self
                return cell
            case 3:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.homeTableViewCell) as? HomeTableViewCell else{ return UITableViewCell() }
                cell.popular(movies: homeModel)
                cell.backgroundColor = .black
                cell.index = indexPath.section
                cell.onClickSeeAllClosure = {index in
                    if let indexSA = index {
                        self.moveSeeAll(index: indexSA)
                    }
                }
                cell.delegate = self
                return cell
            default:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.homeTableViewCell) as? HomeTableViewCell else{ return UITableViewCell() }
                cell.nowPlaying(movies: homeModel)
                cell.backgroundColor = .black
                cell.index = indexPath.section
                cell.onClickSeeAllClosure = {index in
                    if let indexSA = index {
                        self.moveSeeAll(index: indexSA)
                    }
                }
                cell.delegate = self
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let detailVC = storyboard?.instantiateViewController(identifier: Constants.detailViewController) as? DetailViewController else {return}
        detailVC.homeModel = filteredTableData[indexPath.row]
        navigationController?.pushViewController(detailVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let pagerView = FSPagerView()
        tableView.sectionIndexMinimumDisplayRowCount = 1
        pagerView.dataSource = self
        pagerView.delegate = self
        pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: Constants.FSPagerViewCell)
        pagerView.transformer = FSPagerViewTransformer(type: .linear)
        pagerView.isInfinite = true
        pagerView.itemSize = CGSize(width: 220, height: 180)
        pagerView.automaticSlidingInterval = 3.0
        return pagerView
    }
    
    // MARK: Load JSON
    func loadJson() {
        if let path = Bundle.main.path(forResource: "Movie", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                let jsonData = try JSONSerialization.data(withJSONObject: jsonResult, options: .prettyPrinted)
                let jsonDecoder = JSONDecoder()
                jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
                homeModel = try JSONDecoder().decode([HomeModel].self, from: jsonData)
            } catch {
                
            }
        }
    }
}

extension ViewController: HomeTableViewCellDelegate {
    func didSelectMovie(movie: HomeModel) {
        selectedMovie = movie
        guard let vc = storyboard?.instantiateViewController(identifier: Constants.detailViewController) as? DetailViewController else{return}
        vc.homeModel = selectedMovie
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension ViewController: FSPagerViewDelegate, FSPagerViewDataSource {
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return homeModel.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: Constants.FSPagerViewCell, at: index)
        let defaultUrl = Constants.IMAGES_BASE_URL
        let url = defaultUrl + (homeModel[index].posterPath ?? "")
        cell.imageView?.imageFromServerURL(url, placeHolder: nil)
        return cell
    }
}

extension ViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchString = searchController.searchBar.text!
        if !searchString.isEmpty {
            searchActive = true
            filteredTableData.removeAll()
            for item in homeModel {
                if item.title?.uppercased().contains(searchString.uppercased()) == true{
                    filteredTableData.append(item)
                }
            }
        }
        else {
            searchActive = false
            filteredTableData.removeAll()
            filteredTableData = homeModel
        }
        tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        filteredTableData.removeAll()
        tableView.reloadData()
    }
}

extension ViewController {
    
    func configureSearch() {
        self.searchController = ({
            let searchController = UISearchController(searchResultsController: nil)
            searchController.searchResultsUpdater = self
            searchController.obscuresBackgroundDuringPresentation = false
            searchController.searchBar.placeholder = "Type name of movie"
            searchController.searchBar.sizeToFit()
            searchController.searchBar.searchTextField.textColor = Color.UIColorFromRGB(rgbValue: 0xFFFFFF)
            searchController.searchBar.searchTextField.backgroundColor = Color.UIColorFromRGB(rgbValue: 0x666666)
            searchController.searchBar.barTintColor = Color.UIColorFromRGB(rgbValue: 0x444444)
            tableView.tableHeaderView = searchController.searchBar
            definesPresentationContext = true
            navigationItem.hidesSearchBarWhenScrolling = false
            return searchController
        })()
    }
    
    func moveSeeAll (index: Int) {
        switch index {
        case 0:
            guard let vc = storyboard?.instantiateViewController(identifier: Constants.seeAllViewController) as? SeeAllViewController else { return}
            vc.nowPlaying(movies: homeModel)
            navigationController?.pushViewController(vc, animated: true)
        case 1:
            guard let vc = storyboard?.instantiateViewController(identifier: Constants.seeAllViewController) as? SeeAllViewController else { return}
            vc.highestRated(movies: homeModel)
            navigationController?.pushViewController(vc, animated: true)
        case 2:
            guard let vc = storyboard?.instantiateViewController(identifier: Constants.seeAllViewController) as? SeeAllViewController else { return}
            vc.upComing(movies: homeModel)
            navigationController?.pushViewController(vc, animated: true)
        case 3:
            guard let vc = storyboard?.instantiateViewController(identifier: Constants.seeAllViewController) as? SeeAllViewController else { return}
            vc.popular(movies: homeModel)
            navigationController?.pushViewController(vc, animated: true)
        default:
            guard let vc = storyboard?.instantiateViewController(identifier: Constants.seeAllViewController) as? SeeAllViewController else { return}
            vc.nowPlaying(movies: homeModel)
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func headerTable() {
        self.pagerView = ({
            pagerView.dataSource = self
            pagerView.delegate = self
            pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: Constants.FSPagerViewCell)
            pagerView.transformer = FSPagerViewTransformer(type: .linear)
            pagerView.isInfinite = true
            pagerView.itemSize = CGSize(width: 220, height: 180)
            pagerView.automaticSlidingInterval = 3.0
            tableView.tableHeaderView = pagerView
            return pagerView
        })()
    }
    
}

