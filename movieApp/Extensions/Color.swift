//
//  Color.swift
//  movieApp
//
//  Created by Nguyen Huy Dong on 8/17/21.
//

import Foundation
import FSPagerView

struct Color {
     static func UIColorFromRGB(rgbValue: UInt) -> UIColor{
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
