//
//  Constant.swift
//  movieApp
//
//  Created by Nguyen Huy Dong on 8/15/21.
//

import Foundation

struct Constants {
    static let IMAGES_BASE_URL = "https://image.tmdb.org/t/p/w500"
    static let homeCollectionViewCell = "HomeCollectionViewCell"
    static let favouriteCollectionViewCell = "FavouriteCollectionViewCell"
    static let homeTableViewCell = "HomeTableViewCell"
    static let searchTableViewCell = "SearchTableViewCell"
    static let viewController = "ViewController"
    static let detailViewController = "DetailViewController"
    static let favouritesViewController = "FavouritesViewController"
    static let FSPagerViewCell = "FSPagerViewCell"
    static let seeAllViewController = "SeeAllViewController"
    static let seeAllTableViewCell = "SeeAllTableViewCell"
    static let loginViewController = "LoginViewController"
    static let signUpViewController = "SignUpViewController"
    static let homeTabBar = "homeTabBarController"
    static let profileViewController = "ProfileViewController"
    
}
