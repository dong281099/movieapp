//
//  LoadJSON.swift
//  movieApp
//
//  Created by Nguyen Huy Dong on 8/15/21.
//

import Foundation

public class APIService {
    @Published var homeModel = [HomeModel]()
    init() {
        loadJson()
    }
     func loadJson() {
        if let path = Bundle.main.url(forResource: "Movie", withExtension: "json") {
            do {
                let data = try Data(contentsOf: path, options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                let jsonData = try JSONSerialization.data(withJSONObject: jsonResult, options: .prettyPrinted)
                let jsonDecoder = JSONDecoder()
                jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
                homeModel = try JSONDecoder().decode([HomeModel].self, from: jsonData)
            } catch {
                print(error)
            }
        }
    }
}
